Apple Orchard
==============

Introduction
------------
Weather Alerts is an Android application that allows users to search the Apple iTunes store for movies, music, and software.

Developed from scratch as part of related coursework at Ball State University, this project accepts user input via a GUI, queries a web-based data source, and returns query results to the user.

Technologies
------------
Java
Android Studio
BitBucket
JSON

Requirements
------------
+ Android device running Android 6.0+
+ Device permission to save files.
+ Device permission to alter application installation restrictions.
+ An active internet connection.

Installation
------------
This application is not hosted in the Google Play store and must be 'sideloaded' to a device. To sideload the application, follow the steps below:

1. Locate the application's .apk from the source repository at app/release/AppleOrchard.apk .
2. Copy AppleOrchard.apk to the target device's local storage.
3. Using the device, navigate to the .apk file and open it.
4. If prompted, allow the installation of unknown apps on the target device.

Use
---
1. To start the application, tap the application's "Android" icon from the devices app library.
2. Enter the title of the movie, song, or application for which you would like to search.
3. From the drop-down menu to the right of the search terms entered, select if you are searching for a movie, music, or software.
4. Choose "SEARCH"
5. From the search results returned, tap on a result to view more details.
6. Use the device's "back" button to return to the search results.


Concepts Demonstrated
---------------------
+ Familiarity with the Java programming language.
+ Understanding and execution of Model-View-Controller (MVC) program architecture.
+ Understanding of object-oriented programming practices, including the creation and use of objects.
+ Delivery of "clean code", including the separation of functionality into classes, minimizing method sizes, choice of reasonable method and variable names, consistency in code formatting, appropriate use of comments, and the like.
+ Understanding of JSON structure and parsing methods.
+ Request and receipt of data from internet-connected sources.
+ Development and debugging of software in Android Studio, an IntelliJ IDE.
+ Use of version control (Git) during software development.